<?php
namespace atlaxy;

use think\Model;

class Customer extends Model
{
    //创建该用户表

    protected $table = "atlaxy_customer";

        // -- ----------------------------
        // -- Table structure for atlaxy_customer
        // -- ----------------------------
        // DROP TABLE IF EXISTS `atlaxy_customer`;
        // CREATE TABLE `atlaxy_customer` (
        // `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
        // `nickname` varchar(200) COLLATE utf8mb4_bin NOT NULL COMMENT '微信昵称',
        // `gender` tinyint(2) NOT NULL COMMENT '性别',
        // `avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像',
        // `country` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '国家',
        // `province` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '省份',
        // `city` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '城市',
        // `county` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '县区',
        // `openid` varchar(100) COLLATE utf8mb4_bin NOT NULL COMMENT 'OPENID',
        // `login_at` datetime NOT NULL COMMENT '登陆时间',
        // `login_ip` varchar(20) COLLATE utf8mb4_bin NOT NULL COMMENT '登陆IP',
        // `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
        // `delete_time` int(11) NOT NULL DEFAULT '0' COMMENT '软删除',
        // PRIMARY KEY (`id`)
        // ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='默认对customer表';

}